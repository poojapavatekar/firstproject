package com.flipkart.Selenium;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.flipkart.Base.TestBase;


public class Keywords {

	final static Logger logger = Logger.getLogger(Keywords.class);
	WebDriver driver= new TestBase().driver;
	static Properties Config= new TestBase().CONFIG;
	
	//openBrowser
	
	public  void openBrowser(){
		String browser= Config.getProperty("BrowserType");
		String firefoxProfile= Config.getProperty("firefoxProfile");
		int implicitWait= Integer.parseInt(Config.getProperty("implicitWait"));
		logger.info("Browser parameter is : " + browser);
		
		if(browser.equalsIgnoreCase("FF")){
			logger.info("opening Firefox Browser with profile : " + firefoxProfile);
			ProfilesIni prof= new ProfilesIni();
			FirefoxProfile fp=prof.getProfile(firefoxProfile);
			this.driver = new FirefoxDriver(fp);
			
		}else if(browser.equalsIgnoreCase("Chrome")){
			logger.info("opening Chrome Browser");

			System.setProperty("webdriver.chrome.driver", TestBase.PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\drivers\\chromedriver.exe");
			this.driver = new ChromeDriver();
		}else if(browser.equalsIgnoreCase("IE")){
			logger.info("opening IE Browser");
			System.setProperty("webdriver.chrome.driver", TestBase.PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\drivers\\IEDriverServer.exe");
			new Keywords().driver = new InternetExplorerDriver();
		}else{
			logger.info("User gave wrong input , hence opening Chrome browser as default");
			System.setProperty("webdriver.chrome.driver", TestBase.PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\drivers\\chromedriver.exe");
			this.driver = new ChromeDriver();
		
		}
		
		this.driver.manage().timeouts().implicitlyWait(implicitWait,TimeUnit.SECONDS);
		this.driver.manage().window().maximize();
		
	}
	
	
	
	//navigate
	public void navigateToURL(String URL){
		logger.info("Navigating to URL : "+ URL);
		this.driver.navigate().to(URL);
	}
	
	
	// 
	
	public static void type(WebElement elem, String data){
		try{
		logger.info("Typing the data "+ data);
		elem.sendKeys(data);
		}catch(NoSuchElementException e){
			logger.error("The element was not found ", e);
		}
	}
	

	public static void click(WebElement elem){
		elem.click();
	}
	
	
	public static  boolean verifyElementText(WebElement elem, String expectedText){
		String actualTxt= elem.getText();
		
		if(actualTxt.equalsIgnoreCase(expectedText)){
			return true;
		}else{
			return false;
		}
	}
	
	
	//navigateBack
	//navigateForward
	//refreshPage
	//SelectValueByText
	//SelectbyIndex
	
	
	//verifyCheckBoxIsChecked
	public boolean verifyCheckBoxIsChecked(WebElement elem){
		
		if(elem.isSelected()){
			return true;
		}else{
			return false;
		}
	}
	
	
	//verifyCheckBoxIsUnchecked

		//verifyRadioSelected
	//verifyRadioIsNotSelected


	// getElementText
	//isElementPresent
	
	
	//checkCheckBox
	public void checkCheckBox(WebElement elem){
		if(!elem.isSelected()){
			click(elem);
		}
	}
	
	
	//uncheckCheckBox
	//getAlertTxt
	//accepAlert
	//dismissAlert
	
	//switchToFrameByIndex
	//switchToFrameByIdOrName
	////switchToFrame
	//hover
	//pause
	//switchTowindow
	//getAllWindowIds
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
