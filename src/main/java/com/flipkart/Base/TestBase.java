package com.flipkart.Base;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;


import com.flipkart.Selenium.Keywords;
import com.flipkart.Utils.FileUtils;

public class TestBase {

	final static Logger logger = Logger.getLogger(Keywords.class);
	
	public WebDriver driver=null;
	public static Properties CONFIG=null;
	public static final String PROJECT_PATH=System.getProperty("user.dir");
	
	public static void init() throws IOException{
		logger.info("Initialzing the CONFIG files");
		if(CONFIG==null){
		CONFIG= FileUtils.initializzePropertyFile(PROJECT_PATH+"\\src\\main\\resources\\com\\flipkart\\config\\config.properties");
		}
	}

	
}
