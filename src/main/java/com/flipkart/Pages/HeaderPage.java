package com.flipkart.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.flipkart.Selenium.Keywords;

public class HeaderPage {

	
	
	@FindBy(name="q")
	public WebElement searchTxtBox;
	//WebElement searchTxtBox= driver.findElement(By.name("q"))
	
	@FindBy(xpath="//button[@class='vh79eN' and @type='submit']")
	public WebElement searchBtn;
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	
	public void serachProduct(String product){
		Keywords.type(searchTxtBox, product);
		Keywords.click(searchBtn);
		
	}

}
