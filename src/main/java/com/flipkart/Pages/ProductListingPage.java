package com.flipkart.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.flipkart.Selenium.Keywords;

public class ProductListingPage {

	
	
	@FindBy(xpath="//span[@class='W-gt5y']")
	public WebElement searchedTxt;
	
	
	
	/////////////////////////////////////////////////////////////////////////////
	public boolean verifySearchedProduct(String expectedText){
		return Keywords.verifyElementText(searchedTxt, expectedText);
	}
	
}
