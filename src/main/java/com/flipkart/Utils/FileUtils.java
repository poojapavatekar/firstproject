package com.flipkart.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileUtils {
	
	
	public static Properties initializzePropertyFile(String filePath) throws IOException{
		
		Properties prop= new Properties();
		FileInputStream ip= new FileInputStream(new File(filePath));
		prop.load(ip);
		return prop;
		
	}
	
	public static Object[][] getTestData(String filePath, String sheetName) {
		
		Xls_Reader xls= new Xls_Reader(filePath);
		
		int rows= xls.getRowCount(sheetName);
		int cols = xls.getColumnCount(sheetName);
		
		Object[][] data= new Object[rows-1][cols];
		
		for(int i=0;i<rows-1;i++){
			for(int j=0;j<cols;j++){
				data[i][j]=xls.getCellData(sheetName, j, i+2);
			}
		}
		
		return data;
	}
	

}
